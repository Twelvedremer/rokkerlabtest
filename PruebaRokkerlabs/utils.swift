//
//  utils.swift
//  PruebaRokkerlabs
//
//  Created by Momentum Lab 1 on 9/18/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import Foundation

let kFullTimeUTC3 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
let kDefaultDateFormat = "yyyy-MM-dd"
let Kurl = "http://r3l-todo-list.herokuapp.com/"
