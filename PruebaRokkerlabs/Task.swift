//
//  Task.swift
//  PruebaRokkerlabs
//
//  Created by Momentum Lab 1 on 9/18/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import Foundation
import ObjectMapper

class Task {

    var _id: String = ""
    var name: String = ""
    var dueDate: String = ""
    var priority: Int = 0

    func isOverdue()-> Bool{
        if Date() > Date.init(dateString: dueDate){
            return true
        } else {
            return false
        }
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
}

extension Task : Mappable {
    func mapping(map: Map) {
        _id <- map["id"]
        dueDate <- map["dueDate"]
        priority <- map["priority"]
        name <- map["name"]
 
    }
}
