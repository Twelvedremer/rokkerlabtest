//
//  CreatedTaskViewController.swift
//  PruebaRokkerlabs
//
//  Created by Momentum Lab 1 on 9/18/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

class CreatedTaskViewController: UIViewController {

    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var textName: UITextField!
    @IBOutlet weak var dueText: UITextField!
    @IBOutlet weak var priorityText: UITextField!
    let datePickerView = UIDatePicker()
    var platform: Platform!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePickerView.locale = NSLocale(localeIdentifier: "en_GB") as Locale
        datePickerView.datePickerMode = .date
        datePickerView.addTarget(self, action: #selector(onDidChageDate(sender:)), for: .valueChanged)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = kDefaultDateFormat
        self.dueText.inputView = datePickerView
        self.addKeyboardToolBar(textField: textName)
        self.addKeyboardToolBar(textField: dueText)
        self.addKeyboardToolBar(textField: priorityText)
        self.platform.delegateCreatedList = self
        self.cleanField()
        self.indicatorView.stopAnimating()
    }

    func cleanField(){
        self.textName.text = ""
        self.dueText.text = ""
        self.priorityText.text = ""
    }
    
    func onDidChageDate(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = kDefaultDateFormat
        self.dueText.text = dateFormatter.string(from: sender.date)
    }

    @IBAction func touchDatePicker(_ sender: UITextField) {
        if let text = self.dueText.text, text.isEmpty{
            self.dueText.text = text
        }
    }
    
    func addKeyboardToolBar(textField: UITextField) {
        let nextButton: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker))
        let keyboardToolBar = UIToolbar(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: CGFloat(datePickerView.frame.size.width), height: CGFloat(25)))
        keyboardToolBar.sizeToFit()
        keyboardToolBar.barStyle = .default
        keyboardToolBar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil), nextButton]
        textField.inputAccessoryView = keyboardToolBar
    }
    
    func donePicker (sender: UIBarButtonItem){
        self.view.endEditing(true)
    }
    
    @IBAction func createdTask(_ sender: AnyObject) {
        if let textInfo = textName.text, !textInfo.isEmpty,
           let date = dueText.text, !date.isEmpty,
           let priority = priorityText.text, !priority.isEmpty, let priorityInt = Int(priority) {
            if priorityInt >= 0 , priorityInt <= 5{
                self.indicatorView.startAnimating()
                self.platform.addTask(name: textInfo, priority: priorityInt, date: date)
            } else {
                self.presentMessage(title: "Error", body: "only priorities greater than 0 and less than 5 are accepted")
            }
            
        } else {
            self.presentMessage(title: "Error", body: "Exist empty field or values not valid")
        }
    }

    
    func presentMessage(title:String, body: String, event: ((UIAlertAction)-> Void)? = {_ in }){
        let alert = UIAlertController(title: title, message: body, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ok", style: .default, handler: event)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
}

extension CreatedTaskViewController: plataformNewCreatedDelegate{
    func createTask(task: Task?){
        self.indicatorView.stopAnimating()
        if let task = task{
            self.platform.addTaskList(task: task)
            self.presentMessage(title: "", body: "task created successfully", event:{ action in
                _ = self.navigationController?.popViewController(animated: true)
            })
        } else {
            self.presentMessage(title: "Error", body: "Failed to register task on server, try later")
        }
    }
}
