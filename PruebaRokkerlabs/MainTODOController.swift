//
//  ViewController.swift
//  PruebaRokkerlabs
//
//  Created by Momentum Lab 1 on 9/18/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit


let KsegueFilter = "filterView"
let KsegueRegister = "registerView"

class MainTODOController: UIViewController {
    
    let platform = Platform()
    @IBOutlet weak var segmentTab: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.tableView.backgroundColor = .white
        self.platform.delegateTodoList = self
        self.loadingView.startAnimating()
        self.platform.getList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
    }

    @IBAction func changeTab(_ sender: AnyObject) {
        self.platform.changeTags(status: segmentTab.selectedSegmentIndex == 0)
        self.tableView.reloadData()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == KsegueFilter, let vc = segue.destination as? FilterViewController {
            
            vc.platform = self.platform
        } else if segue.identifier == KsegueRegister, let vc = segue.destination as? CreatedTaskViewController{
            vc.platform = self.platform
        }
    }
}

extension MainTODOController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.platform.onlyPending{
            return platform.pending.count
        } else {
            return platform.overdue.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kTodoCell) as! TodoCellTableViewCell
        if self.platform.onlyPending{
            cell.loadInfo(task: platform.pending[indexPath.row])
        } else {
            cell.loadInfo(task: platform.overdue[indexPath.row])
        }
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool{
        return indexPath.row == 0 ? false : true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath){
        if editingStyle == .delete{
            self.loadingView.startAnimating()
            if self.platform.onlyPending{
                self.platform.removeElement(task: platform.pending[indexPath.row])
            } else {
                self.platform.removeElement(task: platform.overdue[indexPath.row])
            }
            
        }
    }
    
}

extension MainTODOController: plataformToDoDelegate{
    func getList(list: [Task]){
        self.platform.changeTaskList(tasks: list)
        self.loadingView.stopAnimating()
        self.tableView.reloadData()
    }
    
    func updateList(status: Bool){
        self.loadingView.stopAnimating()
        if status{
            self.tableView.reloadData()
        } else {
            self.presentMessage(title: "Error", body: "Problem in the server, try later", event: { _ in
                self.tableView.reloadData()
            })
        }
    }
    
    func presentMessage(title:String, body: String, event: ((UIAlertAction)-> Void)? = {_ in }){
        let alert = UIAlertController(title: title, message: body, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ok", style: .default, handler: event)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
}

