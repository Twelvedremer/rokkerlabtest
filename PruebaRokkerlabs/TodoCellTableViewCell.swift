//
//  TodoCellTableViewCell.swift
//  PruebaRokkerlabs
//
//  Created by Momentum Lab 1 on 9/18/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

let kTodoCell = "TodoCellTableViewCell"
class TodoCellTableViewCell: UITableViewCell {

    @IBOutlet weak var taskName: UILabel!
    @IBOutlet weak var dateInfo: UILabel!
    @IBOutlet weak var priorityInfo: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func loadInfo(task: Task){
        self.taskName.text = task.name
        let date = Date.init(dateString: task.dueDate)
        self.dateInfo.text = date.formatDate(format: kDefaultDateFormat)
        self.priorityInfo.text = "priority: \(task.priority)"
        if task.isOverdue(){
            self.taskName.textColor = .red
        } else {
            self.taskName.textColor = .black
        }
    }
    
    
}
