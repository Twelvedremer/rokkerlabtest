//
//  String+utils.swift
//  PruebaRokkerlabs
//
//  Created by Momentum Lab 1 on 9/18/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//


import Foundation

extension String {
    
    static func formatBy(dateString : String, dateFormat : String? = nil) -> String{
        let date = Date(dateString: dateString)
        let formatter = DateFormatter()
        var result: String = ""
        
        if dateFormat != nil {
            formatter.dateFormat = dateFormat
            result = formatter.string(from: date)
        }else{
            formatter.dateStyle = .medium
            result = formatter.string(from: date)
        }
        return result
    }
}
