//
//  utils.swift
//  PruebaRokkerlabs
//
//  Created by Momentum Lab 1 on 9/18/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import Foundation



extension Date{
    /**
     New builder type for date
     - parameter dateString: Date in text format
     - parameter dateFormat: date format, default format kFullTimeUTC3
     */
    init(dateString: String, dateFormat : String = kFullTimeUTC3) {
        let dateStringFormatter = DateFormatter()
        dateStringFormatter.dateFormat = dateFormat
        var date : Date!
        if let dateWithFormat = dateStringFormatter.date(from: dateString){
            date = dateWithFormat
        }else{
            dateStringFormatter.dateFormat = kFullTimeUTC3
            dateStringFormatter.timeZone = TimeZone(abbreviation: "UTC")
            
            if let dateWithFormat = dateStringFormatter.date(from: dateString){
                date = dateWithFormat
            }else{
                dateStringFormatter.dateFormat = kFullTimeUTC3
                dateStringFormatter.timeZone = TimeZone(abbreviation: "UTC")
                
                if let dateWithFormat = dateStringFormatter.date(from: dateString){
                    date = dateWithFormat
                }else{
                   date = Date()
                }
            }
        }
        self.init(timeInterval: 0, since: date)
    }
    

    func formatDate(format: String = kDefaultDateFormat) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
}
