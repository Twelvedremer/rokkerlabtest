//
//  Platform.swift
//  PruebaRokkerlabs
//
//  Created by Momentum Lab 1 on 9/18/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper


protocol plataformToDoDelegate {
    func getList(list: [Task])
    func updateList(status: Bool)
}

protocol plataformNewCreatedDelegate {
    func createTask(task: Task?)
}


class Platform {
    
    var filterOrdering: Int = 3
    var descOrdering: Bool = false
    var onlyPending:Bool = true
    private var tasks:[Task] = []
    var delegateTodoList: plataformToDoDelegate?
    var delegateCreatedList: plataformNewCreatedDelegate?
    
    var pending:[Task] {
        get {
            return tasks.filter { task in
                return !task.isOverdue()
                }.sorted(by: orderingTask)
        }
    }
    
    
    var overdue:[Task] {
        get {
            return tasks.filter { task in
                return task.isOverdue()
                }.sorted(by: orderingTask)
        }
    }
    
    func orderingTask(a:Task, b: Task) -> Bool{
        if self.filterOrdering == 0{
            return !self.descOrdering ? a.name >= b.name: a.name < b.name
        } else if self.filterOrdering == 1{
            return !self.descOrdering ? Date(dateString: a.dueDate) >= Date(dateString: b.dueDate): Date(dateString: a.dueDate) < Date(dateString: b.dueDate)
        } else if self.filterOrdering == 2{
            return !self.descOrdering ? a.priority >= b.priority: a.priority < b.priority
        } else {
            return !self.descOrdering ? a.name >= b.name: a.name < b.name
        }
    }
    
    func changeStatusFilter(filter: Int){
        self.filterOrdering = filter
    }
    
    func getFilterStatus() -> Int{
        return self.filterOrdering
    }
    
    func changeTags(status: Bool){
        self.onlyPending = status
    }
    
    func addTaskList(task: Task){
        self.tasks.append(task)
    }
    
    func changeTaskList(tasks: [Task]){
        self.tasks = tasks
    }
    
    func getList(){
        let networkReach = NetworkReachabilityManager()
        if (networkReach?.isReachable ?? false){
            let url = Kurl + "task"
            Alamofire.request(url, method: .get).responseArray(completionHandler: { (response: DataResponse<[Task]>) in
                let array = response.result.value
                if let result = array{
                    self.delegateTodoList?.getList(list: result)
                } else {
                    self.delegateTodoList?.getList(list: [])
                }
            })
        } else {
            self.delegateTodoList?.getList(list: [])
        }
    }
    
    func addTask(name: String, priority: Int, date: String){
        let networkReach = NetworkReachabilityManager()
        if (networkReach?.isReachable ?? false){
            let url = Kurl + "task/create?name=\(name)&dueDate=\(date)&priority=\(priority)"
            Alamofire.request(url, method: .post).responseObject(completionHandler: { (response: DataResponse<Task>) in
                let response = response.result.value
                if let response = response{
                    self.delegateCreatedList?.createTask(task: response)
                } else {
                    self.delegateCreatedList?.createTask(task: nil)
                }
            })
        } else {
            self.delegateCreatedList?.createTask(task: nil)
        }
    }
    
    func removeElement(task: Task){
        let networkReach = NetworkReachabilityManager()
        if (networkReach?.isReachable ?? false){
            let url = Kurl + "task/destroy/" + task._id
            Alamofire.request(url, method: .get).responseObject(completionHandler: { (response: DataResponse<Task>) in
                let result = response.result.value
                if let result = result{
                    guard let index = self.tasks.index(where: { (task) -> Bool in
                        return task._id == result._id
                    }) else {
                        self.delegateTodoList?.updateList(status: false)
                        return
                    }
                    self.tasks.remove(at: index)
                    self.delegateTodoList?.updateList(status: true)
                }
            })
        } else {
            self.delegateTodoList?.getList(list: [])
        }
    }
    
}





