//
//  FilterViewController.swift
//  PruebaRokkerlabs
//
//  Created by Momentum Lab 1 on 9/18/17.
//  Copyright © 2017 MomentumLab. All rights reserved.
//

import UIKit

class FilterViewController: UIViewController {

    @IBOutlet weak var checkName: UISwitch!
    @IBOutlet weak var checkDue: UISwitch!
    @IBOutlet weak var CheckPriority: UISwitch!
    @IBOutlet weak var checkAscending: UISwitch!
    var platform: Platform!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.checkName.tag = 0
        self.checkDue.tag = 1
        self.CheckPriority.tag = 2
        if self.platform.getFilterStatus() == 0{
            self.checkName.isOn = true
            self.checkDue.isOn = false
            self.CheckPriority.isOn = false
        } else if self.platform.getFilterStatus() == 1{
            self.checkName.isOn = false
            self.checkDue.isOn = true
            self.CheckPriority.isOn = false
        } else if self.platform.getFilterStatus() == 2{
            self.checkName.isOn = false
            self.checkDue.isOn = false
            self.CheckPriority.isOn = true
        } else {
            self.checkName.isOn = false
            self.checkDue.isOn = false
            self.CheckPriority.isOn = false
        }
        self.checkAscending.isOn = self.platform.descOrdering
    }
    
    @IBAction func changeFilter(_ sender: UISwitch) {
        if sender.isOn{
        self.platform.changeStatusFilter(filter: sender.tag)
            if sender.tag == 0{
                self.checkDue.isOn = false
                self.CheckPriority.isOn = false
            } else if sender.tag == 1{
                self.checkName.isOn = false
                self.CheckPriority.isOn = false
            } else if sender.tag == 2{
                self.checkName.isOn = false
                self.checkDue.isOn = false
            }
        } else {
            if !self.checkDue.isOn && !self.checkName.isOn && !self.CheckPriority.isOn{
                self.platform.changeStatusFilter(filter: 3)
            }
        }
    }
   
    @IBAction func alterOrdering(_ sender: UISwitch) {
        self.platform.descOrdering = sender.isOn
    }
}
